def print_tab(*args, tab_width=4):
    pr = ''

    for a in args:
        a = str(a)
        la = len(a)
        pr += a + (' ' * (tab_width - la))

    print(pr)


if __name__ == "__main__":
    for i in range(18):
        b = str(bin(i))[2:]
        print_tab(i, len(b), b)
