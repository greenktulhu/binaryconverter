from ref import print_tab

def int_to_bin(num):
	if num == 0:
		return 0, 1
	power = 0

	bits = 2 ** power
	while (num + 1) > bits:
		power += 1
		bits = 2 ** power

	# for i in range(bits)
	return num, power


ints = [i for i in range(18)]

for i in ints:
	print_tab(int_to_bin(i))
